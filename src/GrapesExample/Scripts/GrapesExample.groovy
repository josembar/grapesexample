package GrapesExample.Scripts

//introducir dependencias al proyecto sin tener que descargar archivos .jar

@Grapes(
        @Grab(group='org.apache.commons',module='commons-lang3',version='3.8.1')
)

import org.apache.ivy.Ivy14
import org.apache.commons.lang3.text.WordUtils

String name = "Jose Manuel Barrantes"
WordUtils wordUtils = new WordUtils()

println wordUtils.initials(name)
